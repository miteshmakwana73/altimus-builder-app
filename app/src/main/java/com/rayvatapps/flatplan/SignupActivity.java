package com.rayvatapps.flatplan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.flatplan.jsonurl.Config;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mContext=SignupActivity.this;

    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextName;

    TextView title,reg1,reg2,titlepage;

    //private Button buttonSignup;
    private ProgressDialog progressDialog;
    private ProgressBar progressBar;

    RequestQueue requestQueue;

    String HttpUrl = Config.URL_REGISTER;

    LinearLayout login,reg;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //initializing views
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextName = (EditText) findViewById(R.id.editTextName);

        reg=(LinearLayout) findViewById(R.id.lnsignup);
        login=(LinearLayout)findViewById(R.id.lnlogin);

        title=(TextView)findViewById(R.id.tvtitle);
        titlepage=(TextView)findViewById(R.id.tvpage);
        reg1=(TextView)findViewById(R.id.btn_signup);
        reg2=(TextView)findViewById(R.id.btn_signup2);
        imgBack=(ImageView)findViewById(R.id.imgback);
        //buttonSignup = (Button) findViewById(R.id.buttonSignup);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        progressDialog = new ProgressDialog(mContext);

        //attaching listener to button
        //buttonSignup.setOnClickListener(this);

        reg.setOnClickListener(this);
        login.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
        editTextEmail.setTypeface(tf);
        editTextPassword.setTypeface(tf);
        editTextName.setTypeface(tf);
        title.setTypeface(tf);
        titlepage.setTypeface(tf);
        reg1.setTypeface(tf);
        reg2.setTypeface(tf);

        requestQueue = Volley.newRequestQueue(mContext);

    }

    private void registerUser(){

        //getting email and password from edit texts
        final String email = editTextEmail.getText().toString().trim();
        final String password  = editTextPassword.getText().toString().trim();
        final String name  = editTextName.getText().toString().trim();

        //checking if email and passwords are empty
        if(TextUtils.isEmpty(name)){
            editTextName.setError("Please enter name");
            return;
        }

        if(TextUtils.isEmpty(email) ){
            editTextEmail.setError("Please enter email");
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            editTextEmail.setError("Enter proper email");
            return;

        }


        if(TextUtils.isEmpty(password)){
            editTextPassword.setError("Please enter password");
            return;
        }

        if (password.length() < 6) {
            editTextPassword.setError("Password too short, enter minimum 6 characters!");
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

                                showToast(msg);

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Intent intent = new Intent(mContext,LoginActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } catch (Exception e) {

                                            }
                                        }
                                    }, 1000);
                            } else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                                e.printStackTrace();
                            }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressDialog.dismiss();

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("username", name);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.lnsignup:
                registerUser();
                break;

            case R.id.lnlogin:
                Intent i = new Intent(mContext, LoginActivity.class);
                //i.putExtra("username", uname);
                startActivity(i);
                finish();
                break;

            case R.id.imgback:
                /*Intent i = new Intent(mContext, LoginActivity.class);
                //i.putExtra("username", uname);
                startActivity(i);*/
                finish();
                break;
        }
    }
}
