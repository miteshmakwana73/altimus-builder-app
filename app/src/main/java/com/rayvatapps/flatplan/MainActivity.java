package com.rayvatapps.flatplan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.shop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {//implements GestureDetector.OnGestureListener, View.OnTouchListener, GestureDetector.OnDoubleTapListener
    private Context mContext=MainActivity.this;
    ImageView image;
    ImageView imgerror;
    RelativeLayout rlerror;
    TextView status;
    WebView wv;
//    GestureDetector gd;

    boolean doubletab = false;

    private List<shop> shopList;
    String HttpUrl = Config.URL_SHOP;
    String HttpUrlBook = Config.URL_BOOK;

    private String floor_id,imageurl,floor_map_name;
    int type=1,looking_count=0,discussion_count=0,finalization_count=0,looking_old_count=0,discussion_old_count=0,finalization_old_count=0,tempLookingCount=0,tempDiscussionCount=0,tempFinalCount=0;

    private ProgressDialog progressDialog;
    Button btnRefresh;
    int finishActivity=0,tempActivityCount=0;
    Boolean dialogOpen=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            floor_id="";
            imageurl="";
            floor_map_name="";
        } else {
            floor_id= String.valueOf(extras.getInt("floor_id"));
            imageurl= String.valueOf(extras.getString("image"));
            floor_map_name= String.valueOf(extras.getString("name"));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(floor_map_name);

        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);

        // Add image
        image = (ImageView) findViewById(R.id.imageView);

        btnRefresh=(Button)findViewById(R.id.btnrefresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRefresh();
            }
        });

        shopList = new ArrayList<>();

//        gd = new GestureDetector(this);

//        setContentView(R.layout.activity_image_map);

        wv = (WebView)findViewById(R.id.webView1);

        checkconnection();

    }

    private void onClickRefresh() {
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.putExtra("floor_id",Integer.parseInt(floor_id));
        intent.putExtra("image",imageurl);
        startActivity(intent);
        finish();
    }

    private void checkconnection() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                rlerror.setVisibility(View.GONE);
                getFloorDetail();

            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to mobile data
                rlerror.setVisibility(View.GONE);
                getFloorDetail();
            }
        } else {
            // not connected to the internet
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }

        /*ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            rlerror.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            // Create your image
//            Picasso.get()  //Here, this is context.
//                    .load(Config.URL_IMAGE+imageurl)  //Url of the image to load.
//                    .into(image);

//            image.setImageResource(R.drawable.altimus_groundfloorplan);

//            ClickableAreasImage clickableAreasImage = new ClickableAreasImage(new PhotoViewAttacher(image), this);

            // Define your clickable area (pixel values: x coordinate, y coordinate, width, height) and assign an object to it
//            List<ClickableArea> clickableAreas = getClickableAreas();
//            clickableAreasImage.setClickableAreas(clickableAreas);

            getFloorDetail();

        } else {
            //no connection
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);
            showToast("No Connection Found");
        }*/
    }

    private void getFloorDetail() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Loading..."); // Setting Message
//        progressDialog.setTitle("ProgressDialog"); // Setting Title
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.setCancelable(false);
        progressDialog.show(); // Display Progress Dialog

//        albumList.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        try {
//                            Log.e("responce",ServerResponse);
//                            albumList.clear();
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(ServerResponse);

                                //we have the array named hero inside the object
                                //so here we are getting that json array
                                JSONArray heroArray = obj.getJSONArray("shops");

                                //now looping through all the elements of the json array
                                int id,pos_x,pos_y,height,width;
                                String floor_id,shop_name,shop_image,info,statusp,created_at,updated_at,looking,discussion,finalization;
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from    json object
                                    id=heroObject.getInt("id");
                                    floor_id=heroObject.getString("floor_id");
                                    shop_name=heroObject.getString("shop_name");
                                    shop_image=heroObject.getString("shop_image");
                                    info=heroObject.getString("description");
//                                    pos_x= Integer.parseInt(heroObject.getString("pos_x"));
//                                    pos_y=Integer.parseInt(heroObject.getString("pos_y"));
//                                    height=Integer.parseInt(heroObject.getString("height"));
//                                    width =Integer.parseInt(heroObject.getString("width"));
                                    statusp=heroObject.getString("status");
                                    created_at=heroObject.getString("created_at");
                                    updated_at=heroObject.getString("updated_at");
                                    looking=heroObject.getString("looking");
                                    discussion=heroObject.getString("discussion");
                                    finalization=heroObject.getString("finalization");

                                    if(looking.equals("null"))
                                        looking="0";
                                    if(discussion.equals("null"))
                                        discussion="0";
                                    if(finalization.equals("null"))
                                        finalization="0";
                                    //adding the hero to herolist
//                                    clickableAreas.add(new ClickableArea(pos_x, pos_y, width, height, new imageclick(id,floor_id,shop_name,info,shop_image,statusp,looking,discussion,finalization)));

                                    shop shop=new shop(id,
                                            floor_id,
                                            shop_name,
                                            info,
                                            shop_image,
                                            statusp,
                                            created_at,
                                            updated_at,
                                            looking,
                                            discussion,
                                            finalization);
                                    /*String.valueOf(pos_x),
                                            String.valueOf(pos_y),
                                            String.valueOf(width),
                                            String.valueOf(height),*/

                                    shopList.add(shop);

//                                    albumList.add(hero);
                                }
                                showWebview();
//                                loadatainWebview();
                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
//                                adapter.notifyDataSetChanged();

//        final List<ClickableArea> clickableAreas = new ArrayList<>();

        //floor 1st and 2nd plan
        //ORIGINAL
       /* clickableAreas.add(new ClickableArea(160, 190, 50, 50, new imageclick(1,floor_id,"111","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(210, 190, 50, 50, new imageclick(1,floor_id,"112","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(260, 190, 50, 50, new imageclick(1,floor_id,"113","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(175, 380, 50, 50, new imageclick(1,floor_id,"110","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(250, 350, 50, 50, new imageclick(1,floor_id,"109","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 350, 50, 50, new imageclick(1,floor_id,"108","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 50, 50, new imageclick(1,floor_id,"107","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 350, 50, 50, new imageclick(1,floor_id,"106","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(455, 350, 50, 50, new imageclick(1,floor_id,"105","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(495, 350, 50, 50, new imageclick(1,floor_id,"104","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(685, 200, 50, 50, new imageclick(1,floor_id,"101","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(685, 270, 50, 65, new imageclick(1,floor_id,"102","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(685, 340, 50, 65, new imageclick(1,floor_id,"103","info","shop image","status","5","8","1")));*/

        //USED OVER GROUND FLOOR
        /*clickableAreas.add(new ClickableArea(165, 215, 50, 50, new imageclick(1,floor_id,"111","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(223, 215, 45, 50, new imageclick(1,floor_id,"112","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(275, 205, 50, 50, new imageclick(1,floor_id,"113","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(175, 370, 50, 50, new imageclick(1,floor_id,"110","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(245, 355, 45, 50, new imageclick(1,floor_id,"109","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(295, 350, 45, 50, new imageclick(1,floor_id,"108","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 45, 50, new imageclick(1,floor_id,"107","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 345, 45, 60, new imageclick(1,floor_id,"106","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(450, 340, 40, 50, new imageclick(1,floor_id,"105","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(508, 345, 45, 50, new imageclick(1,floor_id,"104","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(665, 230, 50, 50, new imageclick(1,floor_id,"101","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 287, 50, 40, new imageclick(1,floor_id,"102","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(680, 340, 50, 65, new imageclick(1,floor_id,"103","info","shop image","status","5","8","1")));*/

        //ALTIMUS - GROUND FLOOR PLAN
        /*clickableAreas.add(new ClickableArea(665, 205, 50, 50, new imageclick(1,floor_id,"1","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 270, 50, 50, new imageclick(1,floor_id,"2","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(675, 340, 55, 55, new imageclick(1,floor_id,"3","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(510, 355, 45, 50, new imageclick(1,floor_id,"4","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(453, 340, 45, 50, new imageclick(1,floor_id,"5","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 340, 45, 50, new imageclick(1,floor_id,"6","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 43, 50, new imageclick(1,floor_id,"7","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 353, 40, 50, new imageclick(1,floor_id,"8","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(240, 360, 45, 55, new imageclick(1,floor_id,"9","info","shop image","status","5","8","1")));*/


        ////USED OVER GROUND FLOOR
        /*clickableAreas.add(new ClickableArea(665, 230, 50, 50, new imageclick(1,floor_id,"1","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 288, 50, 40, new imageclick(1,floor_id,"2","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(675, 340, 55, 55, new imageclick(1,floor_id,"3","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(510, 355, 45, 50, new imageclick(1,floor_id,"4","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(453, 340, 45, 50, new imageclick(1,floor_id,"5","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 340, 45, 50, new imageclick(1,floor_id,"6","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 43, 50, new imageclick(1,floor_id,"7","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 353, 40, 50, new imageclick(1,floor_id,"8","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(240, 360, 45, 55, new imageclick(1,floor_id,"9","info","shop image","status","5","8","1")));*/

        //ALTIMUS - 4TH TO 13TH FLOOR PLAN
        //ORIGINAL
        /*clickableAreas.add(new ClickableArea(665, 205, 55, 50, new imageclick(1,floor_id,"401","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 270, 50, 50, new imageclick(1,floor_id,"402","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(660, 345, 50, 50, new imageclick(1,floor_id,"403","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(510, 340, 40, 50, new imageclick(1,floor_id,"404","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(453, 340, 45, 50, new imageclick(1,floor_id,"405","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 340, 45, 50, new imageclick(1,floor_id,"406","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 43, 50, new imageclick(1,floor_id,"407","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 353, 40, 50, new imageclick(1,floor_id,"408","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(250, 360, 40, 50, new imageclick(1,floor_id,"409","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(175, 370, 50, 50, new imageclick(1,floor_id,"410","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(165, 210, 50, 50, new imageclick(1,floor_id,"411","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(225, 205, 45, 50, new imageclick(1,floor_id,"412","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(280, 200, 50, 50, new imageclick(1,floor_id,"413","info","shop image","status","5","8","1")));*/


        ////USED OVER GROUND FLOOR
       /* clickableAreas.add(new ClickableArea(665, 230, 55, 50, new imageclick(1,floor_id,"401","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 288, 50, 40, new imageclick(1,floor_id,"402","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(655, 350, 55, 50, new imageclick(1,floor_id,"403","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(508, 345, 45, 50, new imageclick(1,floor_id,"404","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(453, 340, 45, 50, new imageclick(1,floor_id,"405","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 340, 45, 50, new imageclick(1,floor_id,"406","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 43, 50, new imageclick(1,floor_id,"407","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 353, 40, 50, new imageclick(1,floor_id,"408","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(247, 360, 40, 50, new imageclick(1,floor_id,"409","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(175, 370, 50, 50, new imageclick(1,floor_id,"410","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(165, 230, 50, 50, new imageclick(1,floor_id,"411","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(225, 230, 45, 50, new imageclick(1,floor_id,"412","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(280, 220, 50, 50, new imageclick(1,floor_id,"413","info","shop image","status","5","8","1")));*/

        //ALTIMUS - 3RD FLOOR PLAN
        //ORIGINAL
        /*clickableAreas.add(new ClickableArea(665, 205, 55, 50, new imageclick(1,floor_id,"301","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 270, 50, 50, new imageclick(1,floor_id,"302","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(658, 345, 50, 50, new imageclick(1,floor_id,"303","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(510, 345, 40, 50, new imageclick(1,floor_id,"304","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(453, 340, 45, 50, new imageclick(1,floor_id,"305","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 340, 45, 50, new imageclick(1,floor_id,"306","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 43, 50, new imageclick(1,floor_id,"307","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 353, 40, 50, new imageclick(1,floor_id,"308","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(248, 360, 40, 55, new imageclick(1,floor_id,"309","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(175, 370, 50, 50, new imageclick(1,floor_id,"310","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(165, 210, 50, 50, new imageclick(1,floor_id,"311","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(225, 205, 45, 50, new imageclick(1,floor_id,"312","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(280, 200, 50, 50, new imageclick(1,floor_id,"313","info","shop image","status","5","8","1")));*/


        ////USED OVER GROUND FLOOR
        /*clickableAreas.add(new ClickableArea(665, 230, 55, 50, new imageclick(1,floor_id,"301","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(665, 287, 50, 40, new imageclick(1,floor_id,"302","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(658, 345, 55, 50, new imageclick(1,floor_id,"303","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(510, 345, 40, 50, new imageclick(1,floor_id,"304","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(453, 340, 45, 50, new imageclick(1,floor_id,"305","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(400, 340, 45, 50, new imageclick(1,floor_id,"306","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(350, 350, 43, 50, new imageclick(1,floor_id,"307","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(300, 353, 40, 50, new imageclick(1,floor_id,"308","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(248, 360, 40, 55, new imageclick(1,floor_id,"309","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(175, 370, 50, 50, new imageclick(1,floor_id,"310","info","shop image","status","5","8","1")));

        clickableAreas.add(new ClickableArea(165, 230, 50, 50, new imageclick(1,floor_id,"311","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(225, 230, 45, 50, new imageclick(1,floor_id,"312","info","shop image","status","5","8","1")));
        clickableAreas.add(new ClickableArea(280, 220, 50, 50, new imageclick(1,floor_id,"313","info","shop image","status","5","8","1")));*/

                            } else {
                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            status.setText(R.string.nodata);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }
                        // Showing response message coming from server.
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        if( volleyError instanceof NoConnectionError) {
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                            status.setText(R.string.connection_slow);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                            status.setText(R.string.server_error);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("floor_id", floor_id);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);

    }

    private void showWebview() {
        WebSettings setting = wv.getSettings();
        setting.setBuiltInZoomControls(true);
        setting.setSupportZoom(true);
        setting.setUseWideViewPort(true);
        setting.setLoadWithOverviewMode(true);
        setting.setJavaScriptEnabled(true);
//        setting.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        wv.setInitialScale(1);
        setting.setLoadWithOverviewMode(true);
        setting.setUseWideViewPort(true);
//        wv.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");

//        wv.setOnTouchListener(this);
        Log.e(floor_id,floor_id);
        String url="http://192.168.1.3:8011/map/"+floor_id;
        wv.loadUrl(url);
//        webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
//        wv.loadUrl("http://www.google.com");
        wv.setWebViewClient(new WebViewController());
    }

    private void showDialog(final int shop_id, final String shop_name, String info, String shop_image, final String looking, final String discussion, final String finalization) {
        dialogOpen=false;
        type=1;
        final Dialog dialog = new Dialog(mContext,R.style.mydialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_room_second);
        dialog.setCancelable(false);
//        dialog.setTitle("Feedback");
//        dialog.setCanceledOnTouchOutside(false);

        /*final Dialog dialog = new Dialog(mContext,R.style.mydialog);
        dialog.setContentView(R.layout.dialog_room_new);
        // set the custom dialog components - text, image and button
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_room_new, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);*/

        final LinearLayout lnlooking=(LinearLayout)dialog.findViewById(R.id.lnlooking);
        final LinearLayout lndiscussion=(LinearLayout)dialog.findViewById(R.id.lndiscussion);
        final LinearLayout lnfinalization=(LinearLayout)dialog.findViewById(R.id.lnfinalization);

        final ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgclose);
        final ImageView imgFloor = (ImageView) dialog.findViewById(R.id.imgfloor);
        final TextView tvRoomNo = (TextView) dialog.findViewById(R.id.tvroomno);
        final TextView tvRoomDetails = (TextView) dialog.findViewById(R.id.tvroomdetails);
        final TextView tvLooking = (TextView) dialog.findViewById(R.id.tvlooking);
        final TextView tvLookingCount = (TextView) dialog.findViewById(R.id.tvlookingcount);
        final TextView tvDiscussionCount = (TextView) dialog.findViewById(R.id.tvdiscussioncount);
        final TextView tvFinalizationCount = (TextView) dialog.findViewById(R.id.tvfinalizationcount);
        final LinearLayout lnlookingcountoperation=(LinearLayout)dialog.findViewById(R.id.lnloockingcountoperation);
        final LinearLayout lndiscussioncountoperation=(LinearLayout)dialog.findViewById(R.id.lndiscussioncountoperation);
        final LinearLayout lnfinalizationcountoperation=(LinearLayout)dialog.findViewById(R.id.lnfinalizationcountoperation);
        LinearLayout lncountlooking=(LinearLayout)dialog.findViewById(R.id.lncountlooking);
        LinearLayout lncountdiscussion=(LinearLayout)dialog.findViewById(R.id.lncountdiscussion);
        LinearLayout lncountfinalization=(LinearLayout)dialog.findViewById(R.id.lncountfinalization);
        final ImageView imglooking = (ImageView) dialog.findViewById(R.id.imglooking);
        final ImageView imgpluslooking = (ImageView) dialog.findViewById(R.id.imglookingadd);
        final ImageView imgminuslooking = (ImageView) dialog.findViewById(R.id.imglookingminus);
        final ImageView imgplusdiscussion = (ImageView) dialog.findViewById(R.id.imgdiscussionadd);
        final ImageView imgminusdiscussion = (ImageView) dialog.findViewById(R.id.imgdiscussionminus);
        final ImageView imgplusfinalization = (ImageView) dialog.findViewById(R.id.imgfinalizationadd);
        final ImageView imgminusfinalization = (ImageView) dialog.findViewById(R.id.imgfinalizationminus);
        final ImageView imgplus = (ImageView) dialog.findViewById(R.id.imgplus);
        final ImageView imgminus = (ImageView) dialog.findViewById(R.id.imgimgminus);
        LinearLayout lnBook=(LinearLayout)dialog.findViewById(R.id.lnbook);
        final TextView tvBook=(TextView) dialog.findViewById(R.id.tvbook);

        Glide.with(mContext)
                .load(Config.URL_IMAGE+shop_image)
                .apply(new RequestOptions().placeholder(R.drawable.site1).error(R.drawable.site1))
                .into(imgFloor);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
                dialogOpen=true;
            }
        });
        lnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                int user_id = Integer.parseInt(sharedPreferences.getString("id",""));
                if(looking_count!=0)
                {
                    type=1;
                    finishActivity+=1;
                    submitData(user_id,shop_id,type,looking_count);
                }
                if(discussion_count!=0)
                {
                    finishActivity+=1;
                    type=2;
                    submitData(user_id,shop_id,type,discussion_count);
                }
                if(finalization_count!=0)
                {
                    finishActivity+=1;
                    type=3;
                    submitData(user_id,shop_id,type,finalization_count);
                }
            }
        });

        lnlooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(mContext, view);

                popup.getMenu().add("Looking");
                popup.getMenu().add("Blocked");
                popup.getMenu().add("Booked");

                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
//                Log.e("item id ", String.valueOf(menuItem.getItemId()));
                        tvLooking.setText(menuItem.getTitle());
                        String title=tvLooking.getText().toString();

                        if(title.equals("Looking"))
                        {
                            imglooking.setBackground(getResources().getDrawable(R.drawable.looking));
//                            tvLookingCount.setText(looking);
                            tvBook.setText("Add Looking Person");
                            type=1;
                            if(looking_count==0)
                            {
                                tvLookingCount.setText(looking);
                                looking_old_count=Integer.parseInt(tvLookingCount.getText().toString());
                            }
                            else
                            {
                                tvLookingCount.setText(String.valueOf(tempLookingCount));
                            }
                        }
                        else if(title.equals("Blocked"))
                        {
                            imglooking.setBackground(getResources().getDrawable(R.drawable.discussion));
                            tvBook.setText("Add no Blocked person");
                            type=2;
                            if(discussion_count==0)
                            {
                                tvLookingCount.setText(discussion);
                                discussion_old_count=Integer.parseInt(tvLookingCount.getText().toString());
                            }
                            else
                            {
                                tvLookingCount.setText(String.valueOf(tempDiscussionCount));
                            }
                        }
                        else if(title.equals("Booked"))
                        {
                            imglooking.setBackground(getResources().getDrawable(R.drawable.finalization));
                            tvLookingCount.setText(finalization);
                            tvBook.setText("Add Booked");
                            type=3;
                            if(finalization_count==0)
                            {
                                tvLookingCount.setText(finalization);
                                finalization_old_count=Integer.parseInt(tvLookingCount.getText().toString());
                            }
                            else
                            {
                                tvLookingCount.setText(String.valueOf(tempFinalCount));
                            }

                        }

                        return false;


                    }
                });
//                showPopupstatus(view,tvLooking);
            }
        });

        imgplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current=Integer.parseInt(tvLookingCount.getText().toString());
                int now=current;
                if(type==1)
                {
                    looking_count+=1;
                    now=current+1;
                    tempLookingCount=Integer.parseInt(tvLookingCount.getText().toString())+1;
                }else  if(type==2)
                {
                    discussion_count+=1;
                    now=current+1;
                    tempDiscussionCount=Integer.parseInt(tvLookingCount.getText().toString())+1;
                }
                else  if(type==3)
                {
                    if(current==0)
                    {
                        finalization_count+=1;
                        now=current+1;
                        tempFinalCount=Integer.parseInt(tvLookingCount.getText().toString())+1;
                    }
                }
                if(now<0)
                {
                    tvLookingCount.setText("0");
                }
                else
                {
                    tvLookingCount.setText(String.valueOf(now));
                }
            }
        });

        imgminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current=Integer.parseInt(tvLookingCount.getText().toString());
                int now=current;

                if(type==1)
                {
                    if(current!=looking_old_count)
                    {
                        looking_count-=1;
                        now=current-1;
                        tempLookingCount=Integer.parseInt(tvLookingCount.getText().toString())-1;
                    }

                }else  if(type==2)
                {
                    if(current!=discussion_old_count)
                    {
                        discussion_count-=1;
                        now=current-1;
                        tempDiscussionCount=Integer.parseInt(tvLookingCount.getText().toString())-1;
                    }
                }
                else  if(type==3)
                {
                    if(current!=finalization_old_count)
                    {
                        finalization_count-=1;
                        now=current-1;
                        tempFinalCount=Integer.parseInt(tvLookingCount.getText().toString())-1;
                    }
                }
                if(now<0)
                {
                    tvLookingCount.setText("0");
                }
                else
                {
                    tvLookingCount.setText(String.valueOf(now));
                }
            }
        });
        lncountlooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*lnlookingcountoperation.setVisibility(View.VISIBLE);
                lndiscussioncountoperation.setVisibility(View.GONE);
                lnfinalizationcountoperation.setVisibility(View.GONE);

                lnlooking.setBackground(getResources().getDrawable(R.drawable.cornerborderselected));
                lndiscussion.setBackground(getResources().getDrawable(R.drawable.cornerborder));
                lnfinalization.setBackground(getResources().getDrawable(R.drawable.cornerborder));

                tvBook.setText("Add Looking Person");
                type=1;*/
            }
        });

        lncountdiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnlookingcountoperation.setVisibility(View.GONE);
                lndiscussioncountoperation.setVisibility(View.VISIBLE);
                lnfinalizationcountoperation.setVisibility(View.GONE);

                lnlooking.setBackground(getResources().getDrawable(R.drawable.cornerborder));
                lndiscussion.setBackground(getResources().getDrawable(R.drawable.cornerborderselected));
                lnfinalization.setBackground(getResources().getDrawable(R.drawable.cornerborder));

                tvBook.setText("Add no discussion person");
                type=2;
            }
        });

        lncountfinalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnlookingcountoperation.setVisibility(View.GONE);
                lndiscussioncountoperation.setVisibility(View.GONE);
                lnfinalizationcountoperation.setVisibility(View.VISIBLE);

                lnlooking.setBackground(getResources().getDrawable(R.drawable.cornerborder));
                lndiscussion.setBackground(getResources().getDrawable(R.drawable.cornerborder));
                lnfinalization.setBackground(getResources().getDrawable(R.drawable.cornerborderselected));
                tvBook.setText("Add Finalization");
                type=3;
            }
        });

        imgpluslooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlusValueLooking(tvLookingCount);
            }
        });
        imgminuslooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvLookingCount.getText().equals(looking))
                {
                    getMinusValueLooking(tvLookingCount);
                }
            }
        });

        imgplusdiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlusValueDiscussion(tvDiscussionCount);
            }
        });
        imgminusdiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvDiscussionCount.getText().equals(discussion))
                {
                    getMinusValueDiscussion(tvDiscussionCount);
                }
            }
        });

        imgplusfinalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlusValueFinalization(tvFinalizationCount);
            }
        });
        imgminusfinalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvFinalizationCount.getText().equals(finalization))
                {
                    getMinusValueFinalization(tvFinalizationCount);
                }
            }
        });
        tvRoomNo.setMovementMethod(new ScrollingMovementMethod());
        tvRoomNo.setText(shop_name);
        tvRoomDetails.setText(Html.fromHtml(info).toString());
        tvLookingCount.setText(looking);
        tvDiscussionCount.setText(discussion);
        tvFinalizationCount.setText(finalization);
        looking_old_count=Integer.parseInt(tvLookingCount.getText().toString());

        String finalizationtemp=tvFinalizationCount.getText().toString();
        if(!finalizationtemp.equals("0"))
        {
            lncountlooking.setEnabled(false);
            lncountdiscussion.setEnabled(false);
            lncountfinalization.setEnabled(false);
            lnBook.setEnabled(false);
            tvBook.setText("Sold Out");
            lnBook.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }
        else
        {
            lncountlooking.setEnabled(true);
            lncountdiscussion.setEnabled(true);
            lncountfinalization.setEnabled(true);
            lnBook.setEnabled(true);
        }



//        findByIds(view);  /*HERE YOU CAN FIND YOU IDS AND SET TEXTS OR BUTTONS*/
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//        dialog.setContentView(view);
        final Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        window.setBackgroundDrawableResource(getResources().getColor(R.color.transparent));
        window.setGravity(Gravity.CENTER);
        dialog.show();


       /* WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setAttributes(lp);

        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
        dialog.show();*/
    }

    private void submitData(final int user_id, final int shop_id, final int type, final int count) {
//        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar);
//        progressBar.setVisibility(View.VISIBLE);

        if(type==0)
        {
            showToast("Please select category");
            return;
        }

        if(count==0)
        {
            showToast("Please add count");
            return;
        }
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlBook,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {
                                //move to next page

                                tempActivityCount+=1;
                                if(tempActivityCount==finishActivity)
                                {
                                    showToast(msg);
                                    Intent intent = new Intent(mContext, MainActivity.class);
                                    intent.putExtra("floor_id",Integer.parseInt(floor_id));
                                    intent.putExtra("image",imageurl);
                                    intent.putExtra("name",floor_map_name);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }

                            } else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("user_id", String.valueOf(user_id));
                params.put("shop_id", String.valueOf(shop_id));
                params.put("type", String.valueOf(type));
                params.put("no_of_bookings", String.valueOf(count));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getPlusValueLooking(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current+1;
        looking_count+=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getMinusValueLooking(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current-1;
        looking_count-=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getPlusValueDiscussion(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current+1;
        discussion_count+=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getMinusValueDiscussion(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current-1;
        discussion_count-=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getPlusValueFinalization(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current+1;
        finalization_count+=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getMinusValueFinalization(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current-1;
        finalization_count-=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void showPopupstatus(View v, final TextView status) {

        PopupMenu popup = new PopupMenu(this, v);

        popup.getMenu().add("Looking");
        popup.getMenu().add("Blocked");
        popup.getMenu().add("Booked");

        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
//                Log.e("item id ", String.valueOf(menuItem.getItemId()));
                status.setText(menuItem.getTitle());
                String title=status.getText().toString();


                return false;


            }
        });

    }

    public class WebViewController extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.e("New Url",url);

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.e("New Url",url);
//            progressBar.setVisibility(View.VISIBLE);
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.e("New Url",url);
            try
            {
                String[] parts = url.split("#");
                String first = parts[0];
                String second = parts[1];

                if(!second.equals(""))
                {
                    for (int i=0;i<shopList.size();i++)
                    {
                        final shop shop= shopList.get(i);
                        String name=shop.getShop_name();
                        Log.e("Member",name);
                        if(name.equals(second))
                        {
                            if(dialogOpen)
                            {
                                showDialog(shop.getId(),shop.getShop_name(),shop.getDescription(),shop.getShop_image(),shop.getLooking(),shop.getDiscussion(),shop.getFinalization());
                            }
                        }
                    }
                }

//                Toast.makeText(mContext, second, Toast.LENGTH_SHORT).show();
            }
            catch (IndexOutOfBoundsException e)
            {
                System.out.println(e.getMessage());
            }
//            progressBar.setVisibility(View.GONE);
        }

    }

}