package com.rayvatapps.flatplan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.flatplan.adapter.BuilderAdapter;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.builder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BuilderActivity extends AppCompatActivity {
    private Context mContext=BuilderActivity.this;

    private RecyclerView recyclerView;
    private BuilderAdapter adapter;
    private List<builder> albumList;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView imgerror;
    RelativeLayout rlerror;
    TextView status;
    SwipeRefreshLayout sw_refresh;

    String HttpUrl = Config.URL_BUILDERS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_builder);
        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();
        adapter = new BuilderAdapter(mContext,albumList);
        mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();

    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (getResources().getConfiguration().orientation) {
            case 1:
                mLayoutManager = new GridLayoutManager(mContext, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
            case 2:
                mLayoutManager = new GridLayoutManager(mContext, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
        }
    }

    private void checkconnection() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                rlerror.setVisibility(View.GONE);
                loadBuilder();

            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to mobile data
                rlerror.setVisibility(View.GONE);
                loadBuilder();
            }
        } else {
            // not connected to the internet
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }
        /*ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        Log.e(wifi+" "+datac,String.valueOf(wifi.isConnected()+" "+datac.isConnected()));
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            rlerror.setVisibility(View.GONE);
            loadBuilder();

        } else {
            //no connection
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }*/
    }

    private void loadBuilder() {
        //getting the progressbar

        //making the progressbar visible
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        albumList.clear();

        //creating a string request to send request to the url
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressBar.setVisibility(View.INVISIBLE);

                        try {
//                            Log.e("Data",response);

                            //getting the whole json object from the response
                            JSONObject obj = new JSONObject(response);

                            //we have the array named hero inside the object
                            //so here we are getting that json array
                            JSONArray heroArray = obj.getJSONArray("builders");

                            //now looping through all the elements of the json array
                            for (int i = 0; i < heroArray.length(); i++) {
                                //getting the json object of the particular index inside the array
                                JSONObject heroObject = heroArray.getJSONObject(i);

                                //creating a hero object and giving them the values from json object
                                builder hero = new builder(
                                        heroObject.getInt("id"),
                                        heroObject.getString("name"),
                                        heroObject.getString("image"));

                                albumList.add(hero);
                            }

                            /*for (int i=1;i<60;i++)
                            {
                                builder hero = new builder(3, "Mitesh "+i);

                                albumList.add(hero);
                            }*/

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            status.setText(R.string.nodata);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //displaying the error in toast if occurrs
                        progressBar.setVisibility(View.INVISIBLE);
                        Log.e("error", String.valueOf(volleyError));
                        showToast("Connection slow");
//                        checkconnection();
                        if( volleyError instanceof NoConnectionError) {
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                            status.setText(R.string.connection_slow);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                            status.setText(R.string.server_error);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);

                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                        //+error.toString()
                    }
                });

        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        //adding the string request to request queue
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                Intent intent = new Intent(this,LoginActivity.class);
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                startActivity(intent);
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
