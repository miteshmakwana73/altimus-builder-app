package com.rayvatapps.flatplan;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.flatplan.adapter.BuilderAdapter;
import com.rayvatapps.flatplan.adapter.TrendingAdapter;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.builder;
import com.rayvatapps.flatplan.model.shop;
import com.rayvatapps.flatplan.model.trending;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrendingShopActivity extends AppCompatActivity {
    private Context mContext=TrendingShopActivity.this;

    private RecyclerView recyclerView;
    private TrendingAdapter adapter;
    private List<trending> trendingList;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView imgerror;
    RelativeLayout rlerror;
    TextView status;
    SwipeRefreshLayout sw_refresh;

    String HttpUrl = Config.URL_TRENDING_SHOP;
    private String site_id,site_name,activity;
    int category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending_shop);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            site_id="";
            site_name="";
            category=0;
        } else {
            category= extras.getInt("category");
            site_id= extras.getString("site_id");
            site_name= extras.getString("site_name");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(site_name);

        if(category==1)
        {
            HttpUrl = Config.URL_TRENDING_SHOP;
            activity="Trending Shop";
            getSupportActionBar().setTitle(site_name+" "+activity);
        }
        else if(category==2)
        {
            HttpUrl = Config.URL_SOLD_SHOP;
            activity="Sold Shop";
            getSupportActionBar().setTitle(site_name+" "+activity);
        }

        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        trendingList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        trendingList = new ArrayList<>();
        adapter = new TrendingAdapter(mContext,trendingList,category);
        mLayoutManager = new GridLayoutManager(mContext, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();

    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                rlerror.setVisibility(View.GONE);
                loadTrendingShop();

            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to mobile data
                rlerror.setVisibility(View.GONE);
                loadTrendingShop();
            }
        } else {
            // not connected to the internet
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }

        /*ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            rlerror.setVisibility(View.GONE);
            loadTrendingShop();

        } else {
            //no connection
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }*/
    }

    private void loadTrendingShop() {
        //getting the progressbar

        //making the progressbar visible
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        trendingList.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        try {
//                            Log.e("responce",ServerResponse);
//                            albumList.clear();
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(ServerResponse);

                                //we have the array named hero inside the object
                                //so here we are getting that json array
                                JSONArray heroArray = null;// obj.getJSONArray("trending_shops");

                                if(category==1)
                                {
                                    heroArray = obj.getJSONArray("trending_shops");
                                }
                                else if(category==2)
                                {
                                    heroArray = obj.getJSONArray("sold_shops");
                                }

                                //now looping through all the elements of the json array
                                int pos_x,pos_y,height,width;
                                String bookings,shop_id,floor_id = null,shop_name = null,shop_image = null,info = null,looking,discussion,finalization;
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from    json object
                                    bookings=heroObject.getString("no_of_bookings");
                                    shop_id=heroObject.getString("shop_id");
                                    looking=heroObject.getString("looking");
                                    discussion=heroObject.getString("discussion");
                                    finalization=heroObject.getString("finalization");

                                    JSONArray nestedArray = heroObject.getJSONArray("shop_info");

                                    for (int j = 0; j < nestedArray.length(); j++) {
                                        JSONObject nestedObject = nestedArray.getJSONObject(j);

                                        floor_id=nestedObject.getString("floor_id");
                                        shop_name=nestedObject.getString("shop_name");
                                        info=nestedObject.getString("description");
                                        shop_image=nestedObject.getString("shop_image");

                                    }

                                    if(looking.equals("null"))
                                        looking="0";
                                    if(discussion.equals("null"))
                                        discussion="0";
                                    if(finalization.equals("null"))
                                        finalization="0";
                                    //adding the hero to herolist

                                    if(category==1)
                                    {
                                        if(finalization.equals("0"))
                                        {
                                            trending trending=new trending(shop_id,
                                                    bookings,
                                                    floor_id,
                                                    shop_name,
                                                    info,
                                                    shop_image,
                                                    looking,
                                                    discussion,
                                                    finalization,
                                                    site_id,
                                                    site_name);

                                            trendingList.add(trending);

                                        }
                                    }
                                    else if(category==2)
                                    {
                                        trending trending=new trending(shop_id,
                                                bookings,
                                                floor_id,
                                                shop_name,
                                                info,
                                                shop_image,
                                                looking,
                                                discussion,
                                                finalization,
                                                site_id,
                                                site_name);

                                        trendingList.add(trending);
                                    }


                                }
                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter.notifyDataSetChanged();

                                if(trendingList.isEmpty())
                                {
                                    status.setText(activity.toLowerCase()+" not available");
                                    imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                                    rlerror.setVisibility(View.VISIBLE);
                                }
                            } else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            status.setText(R.string.nodata);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }
                        // Showing response message coming from server.
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                            status.setText(R.string.connection_slow);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                            status.setText(R.string.server_error);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("site_id", site_id);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}
