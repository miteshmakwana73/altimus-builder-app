package com.rayvatapps.flatplan.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.flatplan.MainActivity;
import com.rayvatapps.flatplan.R;
import com.rayvatapps.flatplan.SiteActivity;
import com.rayvatapps.flatplan.TrendingShopActivity;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.builder;
import com.rayvatapps.flatplan.model.trending;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mitesh Makwana on 08/10/18.
 */
public class TrendingAdapter extends RecyclerView.Adapter<TrendingAdapter.MyViewHolder> {

    private Context mContext;
    private List<trending> albumList;

    Typeface tf;

    String HttpUrlBook = Config.URL_BOOK;

    int type=0,looking_count=0,discussion_count=0,finalization_count=0,looking_old_count=0,discussion_old_count=0,finalization_old_count=0,tempLookingCount=0,tempDiscussionCount=0,tempFinalCount=0;;

    int finishActivity=0,tempActivityCount=0;
    int category=0;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgBuilder;
        public TextView title;
        public RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);

            imgBuilder = (ImageView) view.findViewById(R.id.imgbuilder);
            title = (TextView) view.findViewById(R.id.tvtitle);
            rlMain=(RelativeLayout) view.findViewById(R.id.rlmain);
        }
    }

    public TrendingAdapter(Context mContext, List<trending> albumList,int category) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.category = category;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_trending, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);

        /*Animation animation = null;
        animation = AnimationUtils.loadAnimation(mContext, R.anim.wave);
        animation.setDuration(200);
        holder.rlMain.startAnimation(animation);
        animation = null;*/

        final trending album = albumList.get(position);
        holder.title.setText("Shop Number "+album.getShop_name());

        Glide.with(mContext)
                .load(Config.URL_IMAGE+album.getShop_image())
                .apply(new RequestOptions().placeholder(R.drawable.builder2).error(R.drawable.builder2))
                .into(holder.imgBuilder);

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity)mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

                showDialog(Integer.parseInt(album.getShop_id()),album.getShop_name(),album.getInfo(),album.getShop_image(),album.getLooking(),album.getDiscussion(),album.getFinalization(),album.getSite_id(),album.getSite_name());
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    private void showDialog(final int shop_id, final String shop_name, String info, String shop_image, final String looking, final String discussion, final String finalization, final String site_id, final String site_name) {
        type=1;
        final Dialog dialog = new Dialog(mContext,R.style.mydialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_room_second);
//        dialog.setTitle("Feedback");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        /*final Dialog dialog = new Dialog(mContext,R.style.mydialog);
        dialog.setContentView(R.layout.dialog_room_new);
        // set the custom dialog components - text, image and button
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_room_new, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);*/

        final LinearLayout lnlooking=(LinearLayout)dialog.findViewById(R.id.lnlooking);
        final LinearLayout lndiscussion=(LinearLayout)dialog.findViewById(R.id.lndiscussion);
        final LinearLayout lnfinalization=(LinearLayout)dialog.findViewById(R.id.lnfinalization);

        final ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgclose);
        final ImageView imgFloor = (ImageView) dialog.findViewById(R.id.imgfloor);
        final TextView tvRoomNo = (TextView) dialog.findViewById(R.id.tvroomno);
        final TextView tvRoomDetails = (TextView) dialog.findViewById(R.id.tvroomdetails);
        final TextView tvLooking = (TextView) dialog.findViewById(R.id.tvlooking);
        final TextView tvLookingCount = (TextView) dialog.findViewById(R.id.tvlookingcount);
        final TextView tvDiscussionCount = (TextView) dialog.findViewById(R.id.tvdiscussioncount);
        final TextView tvFinalizationCount = (TextView) dialog.findViewById(R.id.tvfinalizationcount);
        final LinearLayout lnlookingcountoperation=(LinearLayout)dialog.findViewById(R.id.lnloockingcountoperation);
        final LinearLayout lndiscussioncountoperation=(LinearLayout)dialog.findViewById(R.id.lndiscussioncountoperation);
        final LinearLayout lnfinalizationcountoperation=(LinearLayout)dialog.findViewById(R.id.lnfinalizationcountoperation);
        LinearLayout lncountlooking=(LinearLayout)dialog.findViewById(R.id.lncountlooking);
        LinearLayout lncountdiscussion=(LinearLayout)dialog.findViewById(R.id.lncountdiscussion);
        LinearLayout lncountfinalization=(LinearLayout)dialog.findViewById(R.id.lncountfinalization);
        final ImageView imglooking = (ImageView) dialog.findViewById(R.id.imglooking);
        final ImageView imgpluslooking = (ImageView) dialog.findViewById(R.id.imglookingadd);
        final ImageView imgminuslooking = (ImageView) dialog.findViewById(R.id.imglookingminus);
        final ImageView imgplusdiscussion = (ImageView) dialog.findViewById(R.id.imgdiscussionadd);
        final ImageView imgminusdiscussion = (ImageView) dialog.findViewById(R.id.imgdiscussionminus);
        final ImageView imgplusfinalization = (ImageView) dialog.findViewById(R.id.imgfinalizationadd);
        final ImageView imgminusfinalization = (ImageView) dialog.findViewById(R.id.imgfinalizationminus);
        final ImageView imgplus = (ImageView) dialog.findViewById(R.id.imgplus);
        final ImageView imgminus = (ImageView) dialog.findViewById(R.id.imgimgminus);
        LinearLayout lnBook=(LinearLayout)dialog.findViewById(R.id.lnbook);
        final TextView tvBook=(TextView) dialog.findViewById(R.id.tvbook);

        Glide.with(mContext)
                .load(Config.URL_IMAGE+shop_image)
                .apply(new RequestOptions().placeholder(R.drawable.site1).error(R.drawable.site1))
                .into(imgFloor);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
                ((Activity)mContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                /*SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyFile", 0);
                category = sharedPreferences.getInt("category",0);*/
                Intent i=new Intent(mContext,TrendingShopActivity.class);
                i.putExtra("category",category);
                i.putExtra("site_id",site_id);
                i.putExtra("site_name",site_name);
                mContext.startActivity(i);
                ((Activity)mContext).finish();
            }
        });
        lnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyFile", 0);
                int user_id = Integer.parseInt(sharedPreferences.getString("id",""));
                if(looking_count!=0)
                {
                    type=1;
                    finishActivity+=1;
                    submitData(user_id,shop_id,type,looking_count,site_id,site_name);
                }
                if(discussion_count!=0)
                {
                    finishActivity+=1;
                    type=2;
                    submitData(user_id,shop_id,type,discussion_count,site_id,site_name);
                }
                if(finalization_count!=0)
                {
                    finishActivity+=1;
                    type=3;
                    submitData(user_id,shop_id,type,finalization_count,site_id,site_name);
                }
            }
        });

        lnlooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(mContext, view);

                popup.getMenu().add("Looking");
                popup.getMenu().add("Blocked");
                popup.getMenu().add("Booked");

                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
//                Log.e("item id ", String.valueOf(menuItem.getItemId()));
                        tvLooking.setText(menuItem.getTitle());
                        String title=tvLooking.getText().toString();

                        if(title.equals("Looking"))
                        {
                            imglooking.setBackground(mContext.getResources().getDrawable(R.drawable.looking));
//                            tvLookingCount.setText(looking);
                            tvBook.setText("Add Looking Person");
                            type=1;
                            if(looking_count==0)
                            {
                                tvLookingCount.setText(looking);
                                looking_old_count=Integer.parseInt(tvLookingCount.getText().toString());
                            }
                            else
                            {
                                tvLookingCount.setText(String.valueOf(tempLookingCount));
                            }
                        }
                        else if(title.equals("Blocked"))
                        {
                            imglooking.setBackground(mContext.getResources().getDrawable(R.drawable.discussion));
                            tvBook.setText("Add no Blocked person");
                            type=2;
                            if(discussion_count==0)
                            {
                                tvLookingCount.setText(discussion);
                                discussion_old_count=Integer.parseInt(tvLookingCount.getText().toString());
                            }
                            else
                            {
                                tvLookingCount.setText(String.valueOf(tempDiscussionCount));
                            }
                        }
                        else if(title.equals("Booked"))
                        {
                            imglooking.setBackground(mContext.getResources().getDrawable(R.drawable.finalization));
                            tvLookingCount.setText(finalization);
                            tvBook.setText("Add Booked");
                            type=3;
                            if(finalization_count==0)
                            {
                                tvLookingCount.setText(finalization);
                                finalization_old_count=Integer.parseInt(tvLookingCount.getText().toString());
                            }
                            else
                            {
                                tvLookingCount.setText(String.valueOf(tempFinalCount));
                            }

                        }

                        return false;


                    }
                });
//                showPopupstatus(view,tvLooking);
            }
        });

        imgplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current=Integer.parseInt(tvLookingCount.getText().toString());
                int now=current;
                if(type==1)
                {
                    looking_count+=1;
                    now=current+1;
                    tempLookingCount=Integer.parseInt(tvLookingCount.getText().toString())+1;
                }else  if(type==2)
                {
                    discussion_count+=1;
                    now=current+1;
                    tempDiscussionCount=Integer.parseInt(tvLookingCount.getText().toString())+1;
                }
                else  if(type==3)
                {
                    if(current==0)
                    {
                        finalization_count+=1;
                        now=current+1;
                        tempFinalCount=Integer.parseInt(tvLookingCount.getText().toString())+1;
                    }
                }
                if(now<0)
                {
                    tvLookingCount.setText("0");
                }
                else
                {
                    tvLookingCount.setText(String.valueOf(now));
                }
            }
        });

        imgminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int current=Integer.parseInt(tvLookingCount.getText().toString());
                int now=current;

                if(type==1)
                {
                    if(current!=looking_old_count)
                    {
                        looking_count-=1;
                        now=current-1;
                        tempLookingCount=Integer.parseInt(tvLookingCount.getText().toString())-1;
                    }

                }else  if(type==2)
                {
                    if(current!=discussion_old_count)
                    {
                        discussion_count-=1;
                        now=current-1;
                        tempDiscussionCount=Integer.parseInt(tvLookingCount.getText().toString())-1;
                    }
                }
                else  if(type==3)
                {
                    if(current!=finalization_old_count)
                    {
                        finalization_count-=1;
                        now=current-1;
                        tempFinalCount=Integer.parseInt(tvLookingCount.getText().toString())-1;
                    }
                }
                if(now<0)
                {
                    tvLookingCount.setText("0");
                }
                else
                {
                    tvLookingCount.setText(String.valueOf(now));
                }
            }
        });

        lncountlooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*lnlookingcountoperation.setVisibility(View.VISIBLE);
                lndiscussioncountoperation.setVisibility(View.GONE);
                lnfinalizationcountoperation.setVisibility(View.GONE);

                lnlooking.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborderselected));
                lndiscussion.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborder));
                lnfinalization.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborder));

                tvBook.setText("Add Looking Person");
                type=1;*/
            }
        });

        lncountdiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnlookingcountoperation.setVisibility(View.GONE);
                lndiscussioncountoperation.setVisibility(View.VISIBLE);
                lnfinalizationcountoperation.setVisibility(View.GONE);

                lnlooking.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborder));
                lndiscussion.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborderselected));
                lnfinalization.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborder));

                tvBook.setText("Add no discussion person");
                type=2;
            }
        });

        lncountfinalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnlookingcountoperation.setVisibility(View.GONE);
                lndiscussioncountoperation.setVisibility(View.GONE);
                lnfinalizationcountoperation.setVisibility(View.VISIBLE);

                lnlooking.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborder));
                lndiscussion.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborder));
                lnfinalization.setBackground(mContext.getResources().getDrawable(R.drawable.cornerborderselected));

                tvBook.setText("Add Finalization");
                type=3;
            }
        });

        imgpluslooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlusValueLooking(tvLookingCount);
            }
        });
        imgminuslooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvLookingCount.getText().equals(looking))
                {
                    getMinusValueLooking(tvLookingCount);
                }
            }
        });

        imgplusdiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlusValueDiscussion(tvDiscussionCount);
            }
        });
        imgminusdiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvDiscussionCount.getText().equals(discussion))
                {
                    getMinusValueDiscussion(tvDiscussionCount);
                }
            }
        });

        imgplusfinalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlusValueFinalization(tvFinalizationCount);
            }
        });
        imgminusfinalization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvFinalizationCount.getText().equals(finalization))
                {
                    getMinusValueFinalization(tvFinalizationCount);
                }
            }
        });
        tvRoomNo.setMovementMethod(new ScrollingMovementMethod());
        tvRoomNo.setText(shop_name);
        tvRoomDetails.setText(Html.fromHtml(info).toString());
        tvLookingCount.setText(looking);
        tvDiscussionCount.setText(discussion);
        tvFinalizationCount.setText(finalization);
        looking_old_count=Integer.parseInt(tvLookingCount.getText().toString());


        String finalizationtemp=tvFinalizationCount.getText().toString();
        if(!finalizationtemp.equals("0"))
        {
            lncountlooking.setEnabled(false);
            lncountdiscussion.setEnabled(false);
            lncountfinalization.setEnabled(false);
            lnBook.setEnabled(false);
            tvBook.setText("Sold Out");
            lnBook.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }
        else
        {
            lncountlooking.setEnabled(true);
            lncountdiscussion.setEnabled(true);
            lncountfinalization.setEnabled(true);
            lnBook.setEnabled(true);
        }

//        dialog.setContentView(view);
        final Window window = dialog.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//        window.setBackgroundDrawableResource(getResources().getColor(R.color.transparent));
        window.setGravity(Gravity.CENTER);
        dialog.show();


    }

    private void submitData(final int user_id, final int shop_id, final int type, final int count, final String site_id, final String site_name) {
//        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar);
//        progressBar.setVisibility(View.VISIBLE);

        if(type==0)
        {
            showToast("Please select category");
            return;
        }

        if(count==0)
        {
            showToast("Please add count");
            return;
        }
        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlBook,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);


                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {
                                //move to next page

                                tempActivityCount+=1;
                                if(tempActivityCount==finishActivity)
                                {
                                    showToast(msg);
                                    Intent intent = new Intent(mContext, TrendingShopActivity.class);
                                    intent.putExtra("site_id",site_id);
                                    intent.putExtra("site_name",site_name);
                                    mContext.startActivity(intent);
                                    ((Activity)mContext).finish();
                                }

                            } else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
//                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("user_id", String.valueOf(user_id));
                params.put("shop_id", String.valueOf(shop_id));
                params.put("type", String.valueOf(type));
                params.put("no_of_bookings", String.valueOf(count));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getPlusValueLooking(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current+1;
        looking_count+=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getMinusValueLooking(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current-1;
        looking_count-=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getPlusValueDiscussion(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current+1;
        discussion_count+=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getMinusValueDiscussion(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current-1;
        discussion_count-=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getPlusValueFinalization(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current+1;
        finalization_count+=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void getMinusValueFinalization(TextView textView) {
        int current=Integer.parseInt(textView.getText().toString());
        int now=current-1;
        finalization_count-=1;
        if(now<0)
        {
            textView.setText("0");
        }
        else
        {
            textView.setText(String.valueOf(now));
        }
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

}