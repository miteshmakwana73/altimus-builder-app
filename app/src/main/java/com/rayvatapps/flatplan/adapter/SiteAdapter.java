package com.rayvatapps.flatplan.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.flatplan.FloorListActivity;
import com.rayvatapps.flatplan.OtherImageActivity;
import com.rayvatapps.flatplan.R;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.site;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mitesh Makwana on 08/09/18.
 */
public class SiteAdapter extends RecyclerView.Adapter<SiteAdapter.MyViewHolder> {

    private Context mContext;
    private List<site> albumList;

    Typeface tf;
    final Random rnd = new Random();
    /*int[] image={R.drawable.inspiquo_0, R.drawable.inspiquo_1,R.drawable.inspiquo_2,R.drawable.inspiquo_3,R.drawable.inspiquo_4,
            R.drawable.inspiquo_5,R.drawable.inspiquo_6,R.drawable.inspiquo_7,R.drawable.inspiquo_8};*/
    ArrayList<String> imageserver,imageList;
    int count,index;

    int status=0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgSite,imgOther;
        public TextView title;
        public RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);

            imgSite = (ImageView) view.findViewById(R.id.imgsite);
            imgOther = (ImageView) view.findViewById(R.id.imgsiteimages);
            title = (TextView) view.findViewById(R.id.tvtitle);
            rlMain=(RelativeLayout) view.findViewById(R.id.rlmain);
        }
    }

    public SiteAdapter(Context mContext, List<site> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_site, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);

        Animation animation = null;
        animation = AnimationUtils.loadAnimation(mContext, R.anim.sliding_animation);
        animation.setDuration(200);
        holder.rlMain.startAnimation(animation);
        animation = null;

        final site album = albumList.get(position);
        holder.title.setText(album.getName());

        Glide.with(mContext)
                .load(Config.URL_IMAGE+album.getImage())
                .apply(new RequestOptions().placeholder(R.drawable.site1).error(R.drawable.site1))
                .into(holder.imgSite);

        holder.imgOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext, OtherImageActivity.class);
                i.putExtra("images",album.getOtherimage());
                mContext.startActivity(i);
            }
        });
        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext,FloorListActivity.class);
                i.putExtra("site_id",album.getId());
                i.putExtra("site_name",album.getName());
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}