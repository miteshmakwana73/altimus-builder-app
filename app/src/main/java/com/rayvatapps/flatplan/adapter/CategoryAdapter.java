package com.rayvatapps.flatplan.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rayvatapps.flatplan.R;
import com.rayvatapps.flatplan.SiteActivity;
import com.rayvatapps.flatplan.TrendingShopActivity;
import com.rayvatapps.flatplan.model.category;

import java.util.List;

/**
 * Created by Mitesh Makwana on 08/10/18.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private List<category> albumList;
    Intent intent;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        LinearLayout layout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tvtitle);
            layout = (LinearLayout) view.findViewById(R.id.lnmain);
        }
    }


    public CategoryAdapter(Context mContext, List<category> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_category, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final category album = albumList.get(position);
        holder.title.setText(album.getTitle());

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(album.getTitle().equals("Trending"))
                {
//                    showToast(album.getTitle());
                    Intent i=new Intent(mContext,TrendingShopActivity.class);
                    i.putExtra("category",1);
                    i.putExtra("site_id",album.getSite_id());
                    i.putExtra("site_name",album.getSite_name());
                    mContext.startActivity(i);
                }
                else if(album.getTitle().equals("Sold"))
                {
//                    showToast(album.getTitle());
                    Intent i=new Intent(mContext,TrendingShopActivity.class);
                    i.putExtra("category",2);
                    i.putExtra("site_id",album.getSite_id());
                    i.putExtra("site_name",album.getSite_name());
                    mContext.startActivity(i);
                }
                else
                {
                    showToast(album.getTitle());
                }
            }
        });
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}