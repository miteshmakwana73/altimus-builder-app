package com.rayvatapps.flatplan.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.flatplan.R;
import com.rayvatapps.flatplan.SiteActivity;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.builder;

import java.util.List;

/**
 * Created by Mitesh Makwana on 07/09/18.
 */
public class BuilderAdapter extends RecyclerView.Adapter<BuilderAdapter.MyViewHolder> {

    private Context mContext;
    private List<builder> albumList;

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgBuilder;
        public TextView title;
        public RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);

            imgBuilder = (ImageView) view.findViewById(R.id.imgbuilder);
            title = (TextView) view.findViewById(R.id.tvtitle);
            rlMain=(RelativeLayout) view.findViewById(R.id.rlmain);
        }
    }

    public BuilderAdapter(Context mContext, List<builder> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_builder, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);

        Animation animation = null;
        animation = AnimationUtils.loadAnimation(mContext, R.anim.wave);
        animation.setDuration(200);
        holder.rlMain.startAnimation(animation);
        animation = null;

        final builder album = albumList.get(position);
        holder.title.setText(album.getName());

        Glide.with(mContext)
                .load(Config.URL_IMAGE+album.getImage())
                .apply(new RequestOptions().placeholder(R.drawable.builder2).error(R.drawable.builder2))
                .into(holder.imgBuilder);

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                int id=album.getId();
                Intent i=new Intent(mContext,SiteActivity.class);
                i.putExtra("builder_id",album.getId());
                i.putExtra("builder_name",album.getName());
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }
}