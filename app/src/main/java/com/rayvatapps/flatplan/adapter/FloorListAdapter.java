package com.rayvatapps.flatplan.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.flatplan.MainActivity;
import com.rayvatapps.flatplan.R;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.floorlist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 08/09/18.
 */
public class FloorListAdapter extends RecyclerView.Adapter<FloorListAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    private List<floorlist> floorList;
    private List<floorlist> floorListFiltered;

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,tvSiteName,tvRoomCount;
        public ImageView imgFloor;
        public RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.tvtitle);
            imgFloor = (ImageView) view.findViewById(R.id.imgfloor);
//            tvSiteName = (TextView) view.findViewById(R.id.tvsite);
            tvRoomCount = (TextView) view.findViewById(R.id.tvroomcount);
            rlMain=(RelativeLayout) view.findViewById(R.id.rlmain);
        }
    }

    public FloorListAdapter(Context mContext, List<floorlist> albumList) {
        this.mContext = mContext;
        this.floorList = albumList;
        this.floorListFiltered = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_floorlist, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.title.setTypeface(tf);
        holder.tvRoomCount.setTypeface(tf);

        Animation animation = null;
        if(position%2==0)
        {
            animation = AnimationUtils.loadAnimation(mContext, R.anim.left_in);
        }
        else
        {
            animation = AnimationUtils.loadAnimation(mContext, R.anim.push_left_in);
        }
        animation.setDuration(200);
        holder.rlMain.startAnimation(animation);
        animation = null;

        final floorlist album = floorListFiltered.get(position);
        holder.title.setText(album.getName());
        holder.tvRoomCount.setText(album.getDescription());

        Glide.with(mContext)
                .load(Config.URL_IMAGE+album.getImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.site1)
                        .error(R.drawable.site1))
                .into(holder.imgFloor);
                /*.diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)*/

        holder.rlMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(mContext,MainActivity.class);
                i.putExtra("floor_id",album.getId());
                i.putExtra("image",album.getImage());
                i.putExtra("name",album.getName());
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return floorListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    floorListFiltered = floorList;
                } else {
                    List<floorlist> filteredList = new ArrayList<>();
                    for (floorlist row : floorList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    floorListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = floorListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                floorListFiltered = (ArrayList<floorlist>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}