package com.rayvatapps.flatplan.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rayvatapps.flatplan.FloorListActivity;
import com.rayvatapps.flatplan.FullSiteImageActivity;
import com.rayvatapps.flatplan.OtherImageActivity;
import com.rayvatapps.flatplan.R;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.site;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Mitesh Makwana on 31/01/19.
 */
public class SiteImageAdapter extends RecyclerView.Adapter<SiteImageAdapter.MyViewHolder> {

    private Context mContext;
    private List<String> imageList;

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgSite;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);

            imgSite = (ImageView) view.findViewById(R.id.imgsiteimage);
            cardView=(CardView) view.findViewById(R.id.card_view);
        }
    }

    public SiteImageAdapter(Context mContext, List<String> imageList) {
        this.mContext = mContext;
        this.imageList = imageList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_site_images, parent, false);
 
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        holder.title.setTypeface(tf);

        final String image = imageList.get(position);

        Glide.with(mContext)
                .load(Config.URL_IMAGE+image)
                .apply(new RequestOptions().placeholder(R.drawable.site1).error(R.drawable.site1))
                .into(holder.imgSite);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor editer = preference.edit();
                editer.putInt("position", position);
                editer.commit();
                Intent i=new Intent(mContext, FullSiteImageActivity.class);
                mContext.startActivity(i);
            }
        });
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }
}