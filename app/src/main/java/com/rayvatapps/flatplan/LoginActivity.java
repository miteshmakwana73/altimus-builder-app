package com.rayvatapps.flatplan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rayvatapps.flatplan.jsonurl.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private Context mContext=LoginActivity.this;

    private EditText inputEmail, inputPassword;
    TextView title,reg1,reg2,titlepage;
    private ProgressBar progressBar;
    //private Button btnSignup, btnLogin, btnReset;

    String email;
    String password;

    String URL_LOGIN = Config.URL_LOGIN;

    LinearLayout login,reg,skip;
    ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        CheckLogin();

        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        title=(TextView)findViewById(R.id.tvtitle);
        titlepage=(TextView)findViewById(R.id.tvpage);
        reg1=(TextView)findViewById(R.id.btn_signup);
        reg2=(TextView)findViewById(R.id.btn_signup2);
        imgBack=(ImageView) findViewById(R.id.imgback);
       /* btnSignup = (Button) findViewById(R.id.btn_signup);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnReset = (Button) findViewById(R.id.btn_reset_password);*/
        login=(LinearLayout) findViewById(R.id.lnlogin);
        reg=(LinearLayout)findViewById(R.id.lnreg);
        skip=(LinearLayout)findViewById(R.id.lnskip);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        inputEmail.setTypeface(tf);
        inputPassword.setTypeface(tf);
        title.setTypeface(tf);
        titlepage.setTypeface(tf);
        reg1.setTypeface(tf);
        reg2.setTypeface(tf);


        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, SignupActivity.class));
                finish();
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, BuilderActivity.class));
                finish();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 email = inputEmail.getText().toString().trim();
                 password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    inputEmail.setError("Enter email address!");
                    return;
                }

               /* if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    inputEmail.setError("Enter proper email address!");
                    return;
                }*/

                if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Enter password!");
                    return;
                }
                checkLogin();

            }
        });
    }

    private void checkLogin() {
        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {
                                //move to next page

                                JSONArray heroarray = jobj.getJSONArray("user_data");

                                //Log.e("Data",response);
                                String stid = null,stusername = null,stpassword = null,stemail = null,ststatus = null,stcreated_at = null,stupdated_at = null;
                                //we have the array named hero inside the object
                                //so here we are getting that json array
                                for(int i=0; i < heroarray.length(); i++) {
                                    JSONObject jsonobject = heroarray.getJSONObject(i);
                                    stid=jsonobject.getString("id");
                                    stusername=jsonobject.getString("name");
                                    stpassword=jsonobject.getString("password");
                                    stemail=jsonobject.getString("email");
                                    ststatus=jsonobject.getString("status");
                                    stcreated_at=jsonobject.getString("created_at");
                                    stupdated_at=jsonobject.getString("updated_at");
                                }

                                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("id",stid);
                                editor.putString("username",stusername);
                                editor.putString("password",stpassword);
                                editor.putString("email",stemail);
                                editor.putString("status",ststatus);
                                editor.putString("created_at",stcreated_at);
                                editor.putString("updated_at",stupdated_at);
                                editor.commit();

                                Intent intent = new Intent(mContext, BuilderActivity.class);
                                startActivity(intent);
                                finish();

                                showToast(msg);

                            } else if (status.equals("2")) {

                                showLocationDialog(msg);

                            }else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void showLocationDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog);
        } else {
            builder = new AlertDialog.Builder(mContext);
        }
        builder.setTitle(getString(R.string.dialog_title));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(msg);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                    }
                });

        /*String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });*/

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void CheckLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        String uname = sharedPreferences.getString("username","");
        if (uname.equalsIgnoreCase("")) {

        }
        else
        {
            Intent intent = new Intent(mContext, BuilderActivity.class);
            startActivity(intent);
            finish();
        }
        /*else if(uname.equalsIgnoreCase("m@m.m")) {
            Intent i = new Intent(LoginActivity.this, TrackMapsActivity.class);
            //i.putExtra("username", uname);
            startActivity(i);
            finish();
        }*/
    }

}
