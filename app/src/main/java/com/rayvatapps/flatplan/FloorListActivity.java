package com.rayvatapps.flatplan;

import android.app.SearchManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rayvatapps.flatplan.adapter.CategoryAdapter;
import com.rayvatapps.flatplan.adapter.FloorListAdapter;
import com.rayvatapps.flatplan.jsonurl.Config;
import com.rayvatapps.flatplan.model.category;
import com.rayvatapps.flatplan.model.floorlist;
import com.rayvatapps.flatplan.util.MyApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FloorListActivity extends AppCompatActivity {
    private Context mContext=FloorListActivity.this;

    private RecyclerView recyclerViewCategory;
    private List<category> categoryList;
    private CategoryAdapter categoryAdapter;

    private RecyclerView recyclerView;
    private FloorListAdapter adapter;
    private List<floorlist> albumList;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView imgerror;
    RelativeLayout rlerror;
    TextView status;
    SwipeRefreshLayout sw_refresh;
    private SearchView searchView;

    String HttpUrl = Config.URL_FLOOR;

    private String site_id,site_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_list);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            site_id="";
            site_name="";
        } else {
            site_id= String.valueOf(extras.getInt("site_id"));
            site_name= extras.getString("site_name");

        }
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(site_name);

        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        albumList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerViewCategory = (RecyclerView) findViewById(R.id.recycler_view_category);

        categoryList = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(mContext, categoryList);

//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
//        recyclerViewCategory.setLayoutManager(mLayoutManager);
//        recyclerViewCategory.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerViewCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewCategory.setAdapter(categoryAdapter);
        getCategory();

        albumList = new ArrayList<>();
        adapter = new FloorListAdapter(mContext,albumList);
        mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void getCategory() {
        category category = new category("Trending",site_id,site_name);
        categoryList.add(category);

        category category1 = new category("Sold",site_id,site_name);
        categoryList.add(category1);

        categoryAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onResume() {
        super.onResume();
        switch (getResources().getConfiguration().orientation) {
            case 1:
                mLayoutManager = new GridLayoutManager(mContext, 1);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
            case 2:
                mLayoutManager = new GridLayoutManager(mContext, 2);
                recyclerView.setLayoutManager(mLayoutManager);
                break;
        }
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                rlerror.setVisibility(View.GONE);
                loadFloorList();

            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to mobile data
                rlerror.setVisibility(View.GONE);
                loadFloorList();
            }
        } else {
            // not connected to the internet
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }

        /*ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {

            rlerror.setVisibility(View.GONE);
            loadFloorList();

        } else {
            //no connection
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }*/
    }

    private void loadFloorList() {
        //getting the progressbar

        //making the progressbar visible
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        albumList.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        try {
//                            Log.e("responce",ServerResponse);
                            albumList.clear();
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(ServerResponse);

                                //we have the array named hero inside the object
                                //so here we are getting that json array
                                JSONArray heroArray = obj.getJSONArray("floors");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object
                                    floorlist hero = new floorlist(
                                            heroObject.getInt("id"),
                                            heroObject.getString("site_id"),
                                            heroObject.getString("name"),
                                            heroObject.getString("description"),
                                            heroObject.getString("image"),
                                            heroObject.getString("map_location"),
                                            heroObject.getString("status"),
                                            site_name);

                                    //adding the hero to herolist
                                    albumList.add(hero);
                                }
                                List<floorlist> items = new Gson().fromJson(heroArray.toString(), new TypeToken<List<floorlist>>() {
                                }.getType());
                               /* List<floorlist> items = new Gson().fromJson(ServerResponse.toString(), new TypeToken<List<floorlist>>() {
                                }.getType());*/

                                //Log.e("list", String.valueOf(albumList));
                             /*   for (int i=1;i<60;i++)
                                {
                                    floorlist hero = new floorlist(i, String.valueOf(i),"Mitesh floor "+i,"sd","sd");

                                    albumList.add(hero);
                                }*/
                                //creating custom adapter object
                                adapter.notifyDataSetChanged();

                            } else {
                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            status.setText(R.string.nodata);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);

                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }
                        // Showing response message coming from server.
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                            status.setText(R.string.connection_slow);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                            status.setText(R.string.server_error);
//                            status.setVisibility(View.VISIBLE);
                            imgerror.setBackground(getResources().getDrawable(R.drawable.no_data));
                            rlerror.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("site_id", site_id);

                return params;
            }

        };

//        // Creating RequestQueue.
//        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
//
//        // Adding the StringRequest object into requestQueue.
//        requestQueue.add(stringRequest);
        MyApplication.getInstance().addToRequestQueue(stringRequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.action_search:
                showToast("Search");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
