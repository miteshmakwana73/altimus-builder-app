package com.rayvatapps.flatplan.jsonurl;

public class Config {
//	public static final String SERVER = "http://192.168.1.3:8011/";
	public static final String SERVER = "http://rayvatapps.com:8011/";

//	public static final String URL_IMAGE = SERVER+"assets/images/";
	public static final String URL_IMAGE = SERVER+"admin/assets/uploads/";

	public static final String URL_LOGIN = SERVER+"login";
	public static final String URL_REGISTER = SERVER+"register";

	public static final String URL_BUILDERS = SERVER+"builders";
	public static final String URL_SITES = SERVER+"sites";
	public static final String URL_FLOOR = SERVER+"floors";
	public static final String URL_SHOP = SERVER+"shops";
	public static final String URL_BOOK = SERVER+"booking";
	public static final String URL_TRENDING_SHOP = SERVER+"trending_shops";
	public static final String URL_SOLD_SHOP = SERVER+"sold_shops";

}