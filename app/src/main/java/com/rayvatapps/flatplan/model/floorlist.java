package com.rayvatapps.flatplan.model;

/**
 * Created by Mitesh Makwana on 08/09/18.
 */
public class floorlist {

    private int id;
    private String site_id;
    private String name;
    private String description;
    private String image;
    private String map_location;
    private String status;
    private String site_name;

    public floorlist() {
    }

    public floorlist(int id, String site_id, String name, String description, String image,String map_location, String status, String site_name) {
        this.id = id;
        this.site_id = site_id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.map_location = map_location;
        this.status = status;
        this.site_name = site_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMap_location() {
        return map_location;
    }

    public void setMap_location(String map_location) {
        this.map_location = map_location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }
}