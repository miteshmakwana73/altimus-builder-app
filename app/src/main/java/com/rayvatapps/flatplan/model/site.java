package com.rayvatapps.flatplan.model;

/**
 * Created by Mitesh Makwana on 08/09/18.
 */
public class site {

    private int id;
    private String name;
    private String image;
    private String otherimage;

    public site(int id, String name, String image, String otherimage) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.otherimage = otherimage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOtherimage() {
        return otherimage;
    }

    public void setOtherimage(String otherimage) {
        this.otherimage = otherimage;
    }
}