package com.rayvatapps.flatplan.model;

public class category {
    private String title;
    private String site_id;
    private String site_name;

    public category(String title, String site_id,String site_name) {
        this.title = title;
        this.site_id = site_id;
        this.site_name = site_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }
}