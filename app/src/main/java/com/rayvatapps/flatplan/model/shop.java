package com.rayvatapps.flatplan.model;

/**
 * Created by Mitesh Makwana on 03/10/18.
 */
public class shop {

    private int id;
    private String floor_id;
    private String shop_name;
    private String description;
    private String shop_image;
    private String status;
    private String created_at;
    private String updated_at;
    private String looking;
    private String discussion;
    private String finalization;

    public shop(int id, String floor_id, String shop_name, String description, String shop_image, String status, String created_at, String updated_at, String looking, String discussion, String finalization) {
        this.id = id;
        this.floor_id = floor_id;
        this.shop_name = shop_name;
        this.description = description;
        this.shop_image = shop_image;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.looking = looking;
        this.discussion = discussion;
        this.finalization = finalization;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFloor_id() {
        return floor_id;
    }

    public void setFloor_id(String floor_id) {
        this.floor_id = floor_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShop_image() {
        return shop_image;
    }

    public void setShop_image(String shop_image) {
        this.shop_image = shop_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getLooking() {
        return looking;
    }

    public void setLooking(String looking) {
        this.looking = looking;
    }

    public String getDiscussion() {
        return discussion;
    }

    public void setDiscussion(String discussion) {
        this.discussion = discussion;
    }

    public String getFinalization() {
        return finalization;
    }

    public void setFinalization(String finalization) {
        this.finalization = finalization;
    }
}