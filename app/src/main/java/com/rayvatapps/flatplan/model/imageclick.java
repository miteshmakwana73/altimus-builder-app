package com.rayvatapps.flatplan.model;

/**
 * Created by Mitesh Makwana on 08/09/18.
 */
public class imageclick {

    private int id;
    private String floor_id;
    private String shop_name;
    private String info;
    private String shop_image;
    private String status;
    private String looking;
    private String discussion;
    private String finalization;


    public imageclick(int id, String floor_id, String shop_name, String info, String shop_image, String status, String looking, String discussion, String finalization) {
        this.id = id;
        this.floor_id = floor_id;
        this.shop_name = shop_name;
        this.info = info;
        this.shop_image = shop_image;
        this.status = status;
        this.looking = looking;
        this.discussion = discussion;
        this.finalization = finalization;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFloor_id() {
        return floor_id;
    }

    public void setFloor_id(String floor_id) {
        this.floor_id = floor_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getShop_image() {
        return shop_image;
    }

    public void setShop_image(String shop_image) {
        this.shop_image = shop_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLooking() {
        return looking;
    }

    public void setLooking(String looking) {
        this.looking = looking;
    }

    public String getDiscussion() {
        return discussion;
    }

    public void setDiscussion(String discussion) {
        this.discussion = discussion;
    }

    public String getFinalization() {
        return finalization;
    }

    public void setFinalization(String finalization) {
        this.finalization = finalization;
    }
}
