package com.rayvatapps.flatplan.model;

/**
 * Created by Mitesh Makwana on 08/10/18.
 */
public class trending {

    private String shop_id;
    private String bookings;
    private String floor_id;
    private String shop_name;
    private String info;
    private String shop_image;
    private String looking;
    private String discussion;
    private String finalization;
    private String site_id;
    private String site_name;

    public trending(String shop_id, String bookings, String floor_id, String shop_name, String info, String shop_image, String looking, String discussion, String finalization,String site_id, String site_name) {
        this.shop_id = shop_id;
        this.bookings = bookings;
        this.floor_id = floor_id;
        this.shop_name = shop_name;
        this.info = info;
        this.shop_image = shop_image;
        this.looking = looking;
        this.discussion = discussion;
        this.finalization = finalization;
        this.site_id = site_id;
        this.site_name = site_name;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getBookings() {
        return bookings;
    }

    public void setBookings(String bookings) {
        this.bookings = bookings;
    }

    public String getFloor_id() {
        return floor_id;
    }

    public void setFloor_id(String floor_id) {
        this.floor_id = floor_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getShop_image() {
        return shop_image;
    }

    public void setShop_image(String shop_image) {
        this.shop_image = shop_image;
    }

    public String getLooking() {
        return looking;
    }

    public void setLooking(String looking) {
        this.looking = looking;
    }

    public String getDiscussion() {
        return discussion;
    }

    public void setDiscussion(String discussion) {
        this.discussion = discussion;
    }

    public String getFinalization() {
        return finalization;
    }

    public void setFinalization(String finalization) {
        this.finalization = finalization;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }
}