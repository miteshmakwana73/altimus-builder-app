package com.rayvatapps.flatplan;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rayvatapps.flatplan.adapter.SiteAdapter;
import com.rayvatapps.flatplan.adapter.SiteImageAdapter;
import com.rayvatapps.flatplan.model.site;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mitesh Makwana on 31/1/19.
 */
public class OtherImageActivity extends AppCompatActivity {
    private Context mContext= OtherImageActivity.this;

    private RecyclerView recyclerView;
    private SiteImageAdapter adapter;
    public static List<String> imageList;
    RecyclerView.LayoutManager mLayoutManager;
    ImageView imgerror;
    RelativeLayout rlerror;
    TextView status;
    SwipeRefreshLayout sw_refresh;

    private String images;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_image);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            images="";
        } else {
            images= extras.getString("images");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Images");

        imageList = Arrays.asList(images.split("\\s*,\\s*"));

        imgerror=(ImageView) findViewById(R.id.imgerror);
        rlerror=(RelativeLayout) findViewById(R.id.rlerror);
        status=(TextView)findViewById(R.id.tvstatus);

        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
//                        albumList.clear();

                        adapter.notifyDataSetChanged();

                        checkconnection();

                        adapter.notifyDataSetChanged();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

//        albumList = new ArrayList<>();
        adapter = new SiteImageAdapter(mContext,imageList);
        mLayoutManager = new GridLayoutManager(mContext, 2);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            rlerror.setVisibility(View.GONE);

        } else {
            //no connection
            status.setText(R.string.noconnection);
            imgerror.setBackground(getResources().getDrawable(R.drawable.no_conncectionp));
            rlerror.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
