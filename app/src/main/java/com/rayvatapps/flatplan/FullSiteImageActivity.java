package com.rayvatapps.flatplan;

import android.Manifest;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.rayvatapps.flatplan.jsonurl.Config;

import java.util.ArrayList;
import java.util.List;

import ooo.oxo.library.widget.PullBackLayout;

/**
 * Created by Mitesh Makwana on 31/01/19.
 */
public class FullSiteImageActivity extends AppCompatActivity implements PullBackLayout.Callback
{
    private String TAG = FullSiteImageActivity.class.getSimpleName();
    private Context mContext=FullSiteImageActivity.this;

    int position=0;
    private static  List<String> imageList;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_site_image);
        imageList = new ArrayList<>();
        // Hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        // Show status bar
//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        /*lblCount = (TextView) v.findViewById(R.id.lbl_count);
        lblTitle = (TextView) v.findViewById(R.id.title);
        lblDate = (TextView) v.findViewById(R.id.date);*/

        imageList=OtherImageActivity.imageList;

        SharedPreferences prefernce = PreferenceManager.getDefaultSharedPreferences(mContext);
        position = prefernce.getInt("position", 0);
//        selectedPosition = getArguments().getInt("position");

        Log.e(TAG, "position: " + position);
        Log.e(TAG, "images size: " + imageList.size());

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        PullBackLayout puller=(PullBackLayout)findViewById(R.id.puller);
        puller.setCallback(this);

        setCurrentItem(position);


    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
//        displayMetaInfo(selectedPosition);
    }

    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            /*displayMetaInfo(position);*/
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    //	adapter
    public class MyViewPagerAdapter extends PagerAdapter /*implements OnClickableAreaClickedListener*/
    {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.raw_image_fullscreen_preview, container, false);

            ImageView imageViewPreview = (ImageView) view.findViewById(R.id.image_preview);

//            ClickableAreasImage clickableAreasImage = new ClickableAreasImage(new PhotoViewAttacher(imageViewPreview), this);

            String image = imageList.get(position);
            Log.e(String.valueOf(imageList.get(position)),String.valueOf(position));

            Glide.with(mContext).load(Config.URL_IMAGE+image)
                    .thumbnail(0.5f)
                    .transition(new DrawableTransitionOptions().crossFade())
//                    .apply(RequestOptions.diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imageViewPreview);



            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }


    @Override
    public void onPullStart() {
        // fade out Action Bar ...
        // show Status Bar ...
    }

    @Override
    public void onPull(float progress) {
        // set the opacity of the window's background
    }

    @Override
    public void onPullCancel() {
        // fade in Action Bar
    }

    @Override
    public void onPullComplete() {
        supportFinishAfterTransition();
    }



}