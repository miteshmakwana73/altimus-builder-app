package com.rayvatapps.flatplan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.crust87.texturevideoview.widget.TextureVideoView;

public class SplashScreen extends AppCompatActivity {
    View mDecorView;
    private Context mContext=SplashScreen.this;

    /*private VideoView videoView;
    MediaPlayer mMediaPlayer;
    int mCurrentVideoPosition;*/

    private TextureVideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mDecorView = getWindow().getDecorView();

        hideSystemUI();

       /* videoView =(VideoView)findViewById(R.id.videoView1);

        //Creating MediaController
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);

        //specify the location of media file
        videoView.setMediaController(new MediaController(this));
//        Uri video = Uri.parse("file:///android_asset/The-strip.mp4");
        Uri uri=Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video1);
//        Uri uri=Uri.parse("file:///android_asset/The-strip.mp4");

        //Setting MediaController and URI, then starting the videoView
        videoView.setMediaController(null);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mMediaPlayer = mediaPlayer;
                // We want our video to play over and over so we set looping to true.
                mMediaPlayer.setLooping(true);
                // We then seek to the current posistion if it has been set and play the video.
                if (mCurrentVideoPosition != 0) {
                    mMediaPlayer.seekTo(mCurrentVideoPosition);
                    mMediaPlayer.start();
                }
            }
        });*/

       /* videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // do nothing here......
                return true;
            }
        });*/
//        Uri uri = Uri.parse("android.resource://com.rayvatapps.flatplan/raw/video1");

        mVideoView = (TextureVideoView) findViewById(R.id.videoClip);

        Uri uri=Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video2);

        mVideoView.setVideoURI(uri);
        mVideoView.start();

        ImageView imageView = (ImageView) findViewById(R.id.centerImage);
        TextView tvTitle = (TextView) findViewById(R.id.tvtitle);
        TextView tvDescription = (TextView) findViewById(R.id.tvdescription);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade);
        imageView.startAnimation(animation);
        tvTitle.startAnimation(animation);

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        tvTitle.setTypeface(tf);
        tvDescription.setTypeface(tf);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(mContext,LoginActivity.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {

                }
            }
        }, 5000);
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Capture the current video position and pause the video.
        try {
//            mCurrentVideoPosition = mMediaPlayer.getCurrentPosition();
//            videoView.pause();
            if (mVideoView != null) {
                mVideoView.stopPlayback();
            }

        }
        catch (NullPointerException e)
        {
            Log.e("Error",e.getMessage().toString());
        }

    }

    /*@Override
    protected void onResume() {
        super.onResume();
        // Restart the video when resuming the Activity
        try {
            videoView.start();
        }
        catch (NullPointerException e)
        {
            Log.e("Error",e.getMessage().toString());
        }
    }*/

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        // When the Activity is destroyed, release our MediaPlayer and set it to null.
        try {

            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        catch (NullPointerException e)
        {
            System.out.print(e.getMessage().toString());
        }
    }*/
}
